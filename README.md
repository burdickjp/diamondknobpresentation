# diamondKnobPresentation 
a Jupyter notebook presentation to accompany [this blog post](https://burdickjp.gitlab.io/2018/09/21/facetedShiftKnob.html). 

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/burdickjp%2Fdiamondknobpresentation/master?filepath=index.ipynb)